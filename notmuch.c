#include <gio/gio.h>
#include <notmuch.h>
#include <stdio.h>

#include "ebstatus.h"

static char *path = NULL;
static const char *symbol;
static const char *sync_symbol;
static const char *query_string;
static char **excludes = { NULL };
static gboolean syncing = FALSE;
static unsigned int message_count = 0;

static void
do_count (void)
{
	notmuch_database_t *db = NULL;
	notmuch_query_t *query;
	notmuch_status_t st;
	char **exclude;

	st = notmuch_database_open_with_config(
			path, NOTMUCH_DATABASE_MODE_READ_ONLY, "", NULL, &db, NULL);
	if (st != NOTMUCH_STATUS_SUCCESS) {
		fprintf(stderr, "Notmuch error: %s\n", notmuch_status_to_string(st));
		goto end;
	}

	query = notmuch_query_create(db, query_string);
	if (query == NULL) goto end;

	for (exclude = excludes; *exclude; exclude++) {
		if (*exclude == 0) continue;
		st = notmuch_query_add_tag_exclude(query, *exclude);
		if (st != NOTMUCH_STATUS_XAPIAN_EXCEPTION) continue;
		fprintf(stderr, "Notmuch error: %s\n", notmuch_status_to_string(st));
		goto end;
	}
	notmuch_query_set_omit_excluded(query, NOTMUCH_EXCLUDE_ALL);

	st = notmuch_query_count_messages(query, &message_count);
	if (st != NOTMUCH_STATUS_SUCCESS)
		fprintf(stderr, "Notmuch error: %s\n", notmuch_status_to_string(st));
end:
	if (db) notmuch_database_destroy(db);
}

static void
refresh_event (void)
{
	if (syncing) return;
	do_count();
	refresh();
}

static void
sync_event (GFileMonitor *monitor, GFile *file, GFile *other, GFileMonitorEvent event_type, gpointer data)
{
	if (!g_file_equal(file, G_FILE(data)))
		return;

	switch (event_type) {
	case G_FILE_MONITOR_EVENT_CREATED:
		syncing = TRUE;
		refresh();
		break;
	case G_FILE_MONITOR_EVENT_DELETED:
		syncing = FALSE;
		do_count();
		refresh();
		break;
	default:
		/* Other events are ignored. */
		(void)monitor; (void)other;
	}
}

static void
xapian_event (GFileMonitor *monitor, GFile *file, GFile *other, GFileMonitorEvent event_type, gpointer data)
{
	char *name;
	(void)monitor; (void)other; (void)data;

	/* Heuristics: we get a CHANGES_DONE event on "flintlock" when a
	 * database change finishes (when the lock is released).
	 */

	if (syncing || event_type != G_FILE_MONITOR_EVENT_CHANGES_DONE_HINT)
		return;
	name = g_file_get_basename(file);
	if (!g_strcmp0(name, "flintlock")) {
		do_count();
		refresh();
	}
	g_free(name);


}

void
notmuch_init ()
{
	const char *variable;
	char *string;
	GKeyFile *config;
	GFile *file, *dir;
	GFileMonitor *monitor;
	GError *error = NULL;

	/* Get the path to the configuration file. */

	variable = g_getenv("NOTMUCH_CONFIG");
	if (variable != NULL) {
		path = g_strdup(variable);
	} else {
		variable = g_getenv("HOME");
		if (variable == NULL) {
			fprintf(stderr, "No $NOTMUCH_CONFIG and $HOME, giving up on notmuch.\n");
			return;
		}
		path = g_strdup_printf("%s/.notmuch-config", variable);
	}

	/* Read the configuration file */

	config = g_key_file_new();
	if (!g_key_file_load_from_file(config, path, G_KEY_FILE_NONE, &error)) {
		fprintf(stderr, "Error reading the notmuch configuration: %s\n", error->message);
		g_error_free(error);
		g_key_file_free(config);
		g_free(path);
		path = NULL;
		return;
	}
	g_free(path);

	/* Get the database path */

	path = g_key_file_get_string(config, "database", "path", NULL);

	if (path == NULL) {
		variable = g_getenv("MAILDIR");
		if (variable != NULL) path = g_strdup(variable);
	}

	if (path == NULL) {
		variable = g_getenv("HOME");
		if (variable != NULL) path = g_strdup_printf("%s/mail", variable);
	}

	if (path == NULL) {
		fprintf(stderr, "Cannot determine the database path for notmuch.\n");
		g_key_file_free(config);
		return;
	}

	/* Get the list of excluded tags */

	string = g_key_file_get_string(config, "search", "exclude_tags", NULL);
	if (string != NULL) {
		excludes = g_strsplit(string, ";", -1);
		g_free(string);
	}

	/* Get the symbol and query */

	symbol = g_key_file_get_string(config, "ebstatus", "symbol", NULL);
	if (symbol == NULL)
		symbol = "";

	query_string = g_key_file_get_string(config, "ebstatus", "query", NULL);
	if (query_string == NULL)
		query_string = "tag:unread";

	/* Make an initial count. */

	do_count();

	/* Watch for changes in the xapian directory or setup a periodic check */

	file = g_file_new_for_path(path);
	dir = g_file_resolve_relative_path(file, ".notmuch/xapian");
	g_object_unref(file);
	monitor = g_file_monitor_directory(dir, G_FILE_MONITOR_NONE, NULL, &error);
	g_object_unref(dir);
	if (monitor == NULL) {
		fprintf(stderr, "Error watching %s: %s\n",
				g_file_peek_path(file), error->message);
		g_error_free(error);
		g_timeout_add_seconds(5, (GSourceFunc)refresh_event, NULL);
	} else {
		g_signal_connect(monitor, "changed", G_CALLBACK(xapian_event), file);
	}

	/* Watch for the sync lock file */

	string = g_key_file_get_string(config, "sync", "lock_file", NULL);
	if (string == NULL)
		goto no_sync;

	file = g_file_new_for_path(string);
	g_free(string);
	dir = g_file_get_parent(file);
	monitor = g_file_monitor_directory(dir, G_FILE_MONITOR_NONE, NULL, &error);
	g_object_unref(dir);
	if (monitor == NULL) {
		fprintf(stderr, "Error watching %s: %s\n",
				g_file_peek_path(file), error->message);
		g_error_free(error);
		goto no_sync;
	}
	syncing = g_file_query_exists(file, NULL);
	g_signal_connect(monitor, "changed", G_CALLBACK(sync_event), file);

	sync_symbol = g_key_file_get_string(config, "ebstatus", "sync_symbol", NULL);
	if (sync_symbol == NULL)
		sync_symbol = "?";
no_sync:

	g_key_file_free(config);
}

gboolean
notmuch_status ()
{
	if (path == NULL || (!syncing && message_count == 0))
		return FALSE;

	g_string_append(status, symbol);
	if (message_count != 0)
		g_string_append_printf(status, "%d", message_count);
	if (syncing)
		g_string_append(status, sync_symbol);

	return TRUE;
}
