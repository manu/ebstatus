#include <gio/gio.h>

#include "ebstatus.h"

static GVolumeMonitor *monitor;
static GList *mounts;

/**
 * add:
 * @monitor: a #GVolumeMonitor
 * @mount: a #GMount
 *
 * Add a mount to the list of active mounts. The mount is added only if it
 * is part of a removable device, i.e. it has an associated drive.
 */

static void
add (GVolumeMonitor *monitor, GMount *mount)
{
	GDrive *drive = g_mount_get_drive(mount);
	(void)monitor;
	if (drive == NULL) return;
	g_object_unref(drive);

	if (!g_mount_can_unmount(mount)) return;

	mounts = g_list_append(mounts, g_object_ref(mount));
	refresh();
}

/**
 * remove:
 * @monitor: a #GVolumeMonitor
 * @mount: a #GMount
 *
 * Remove a mount from the list of active mounts.
 */

static void
remove (GVolumeMonitor *monitor, GMount *mount)
{
	(void)monitor;
	if (g_list_find(mounts, mount)) {
		mounts = g_list_remove(mounts, mount);
		g_object_unref(mount);
		refresh();
	}
}

/**
 * volumes_init:
 *
 * Initialise the GIO Volume status.
 */

void
volumes_init ()
{
	GList *point, *next;
	GDrive *drive;

	monitor = g_volume_monitor_get();
	mounts = g_volume_monitor_get_mounts(monitor);

	/* Filter out mounts that are not part of a removable device. */
	point = mounts;
	while (point != NULL) {
		drive = g_mount_get_drive(point->data);
		if (drive != NULL) {
			g_object_unref(drive);
			point = point->next;
		} else {
			next = point->next;
			g_object_unref(point->data);
			mounts = g_list_delete_link(mounts, point);
			point = next;
		}
	}

	g_signal_connect(monitor, "mount-added",
		G_CALLBACK(add), NULL);
	g_signal_connect(monitor, "mount-removed",
		G_CALLBACK(remove), NULL);
}

/**
 * volumes_status:
 *
 * Print the state of the list of mounted volumes.
 *
 * Returns: %TRUE if some state was printed
 */

gboolean
volumes_status ()
{
	GList *point;
	char *name;

	for (point = mounts; point != NULL; point = point->next) {
		if (point->prev != NULL) g_string_append_c(status, ' ');
		name = g_mount_get_name(point->data);
		g_string_append_printf(status, "%s", name);
		g_free(name);
	}

	return mounts != NULL;
}
