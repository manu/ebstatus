#include <glib.h>
#include <string.h>
#include <pulse/glib-mainloop.h>
#include <pulse/pulseaudio.h>

#include "ebstatus.h"

static const char *symbol;
static gboolean mute;
static int volume;

/**
 * success_cb:
 *
 * A dummy callback for 'success'.
 */

static void
success_cb (pa_context *context, int success, void *data)
{
	(void)context; (void)success; (void)data;
}

/**
 * sink_info_cb:
 * @context: a PulseAudio context
 * @info: a sink info structure
 * @eol: whether this is an "end of list" call
 * @data: unused user data
 *
 * The callback to update sink information. This triggers a refresh of the
 * status.
 */

const char *headphone_port_names[] =
{ "analog-output-headphones", "headset-output", NULL };

static void
sink_info_cb (pa_context *context, const pa_sink_info *info, int eol, void *data)
{
	(void)context; (void)data;
	if (eol) return;

	symbol = "";
	if (info->active_port) {
		const char *name = info->active_port->name;
		const char **candidate = headphone_port_names;
		while (*candidate != NULL && strcmp(name, *candidate))
			++candidate;
		if (*candidate != NULL)
			symbol = "";
	}

	mute = info->mute != 0;
	volume = 100 + 1.1 * pa_sw_volume_to_dB(pa_cvolume_avg(&info->volume));

	refresh();
}

/**
 * subscribe_cb:
 * @context: a PulseAudio context
 * @event: the kind of event received
 * @idx: the sink index
 * @data: unused user data
 *
 * The callback for sink events. Fetches information about the sink.
 */

static void
subscribe_cb (pa_context *context, pa_subscription_event_type_t event, uint32_t idx, void *data)
{
	(void)event; (void)data;
	pa_context_get_sink_info_by_index(context, idx, sink_info_cb, NULL);
}

/**
 * server_info_cb:
 * @context: a PulseAudio context
 * @info: a server info structure
 * @data: unused user data
 *
 * The callback for server info. Fetches information about the default sink.
 */

static void
server_info_cb (pa_context *context, const pa_server_info *info, void *data)
{
	(void)data;
	pa_context_get_sink_info_by_name(context, info->default_sink_name, sink_info_cb, NULL);
}

/**
 * state_cb:
 * @context: a PulseAudio context
 * @data: unused user data
 *
 * The callback for context state changes. Sets up the info callback when the
 * context is ready.
 */

static void
state_cb (pa_context *context, void *data)
{
	(void)data;
	if (pa_context_get_state(context) != PA_CONTEXT_READY)
		return;
	pa_context_get_server_info(context, server_info_cb, NULL);
	pa_context_subscribe(context, PA_SUBSCRIPTION_MASK_SINK, success_cb, NULL);
	pa_context_set_subscribe_callback(context, subscribe_cb, NULL);
}

/**
 * pulseaudio_init:
 *
 * Initialise the PulseAudio volume status.
 */

void
pulseaudio_init ()
{
	pa_glib_mainloop *mainloop;
	pa_context *context;

	mainloop = pa_glib_mainloop_new(g_main_loop_get_context(main_loop));
	context = pa_context_new(pa_glib_mainloop_get_api(mainloop), "ebstatus");
	pa_context_set_state_callback(context, state_cb, NULL);
	pa_context_connect(context, NULL, PA_CONTEXT_NOFLAGS, NULL);
	symbol = NULL;
}

/**
 * pulseaudio_status:
 *
 * Print the status of the active PulseAudio output sink.
 *
 * Returns: %TRUE if some state was printed.
 */

gboolean
pulseaudio_status ()
{
	if (symbol == NULL)
		return FALSE;

	status_start_font("FontAwesome");
	g_string_append(status, symbol);
	status_stop_font();
	if (mute)
		g_string_append(status, "×");
	else
		g_string_append_printf(status, "%d%%", volume);
	return TRUE;
}
